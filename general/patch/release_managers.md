# Release Manager runbook for patch releases

Planned patch releases are scheduled on the second and fourth Wednesdays of the month, around
the release week. The patch release schedule accommodates the GitLab SLO/SLAs:

* [Release SLO]
* [Bug remediation SLO]
* [Vulnerability remediation SLA]

The [release dashboard] provides an overview of the next patch release information. Consider the
patch release schedule is best effort and, as long as the schedule meets the GitLab SLO/SLAs, it can be updated
at the discreation of release managers.

Release managers will perform two patch releases during their shift, if required
they can also performed [unplanned critical patches] or [patch releases for a single version].

## Planned patch releases.

This is the default patch release process that is performed according to the [GitLab Maintenace Policy], 
includes bug fixes for the current GitLab version and security fixes for the current and the two previous
GitLab versions.

The [overview of the planned patch release] explains the end-to-end process.

To perform a planned patch release:

1. Two days before the patch release date, usually on Monday, release managers trigger the ChatOps command
on the `#f_upcoming_release` Slack channel

```
/chatops run release prepare --security 
```

1. A release task issue will be created on the [release task tracker] with the required steps to tag, deploy and
publish the patch release. Release Managers are the DRIs of this issue and it is their responsibility to see it
through and update it as they progress.
1. The patch release will include all the bug fixes merged into the stable branches and process the security fixes
ready at the given time.
1. A blog post will be automatically created on the [www-gitlab-com security] repository by the security pipeline
referenced on the release task issue. AppSec is the DRI of the blog post content.

## Unplanned critical patch release

[Unplanned critical patch releases] are ad-hoc processes used to quickly mitigate a high-severity vulnerability
in order to meet the [Vulnerability remediation SLA]. To trigger a critical unplanned patch release:

```
/chatops run release prepare --security --critical
```

A blog post will be manually prepared by AppSec.

## Patch release for a single version

If required, patch releases can be prepared for a single version. To prepare a patch release for the current version
execute the following command on the `#f_upcoming_release` Slack  channel:

```
/chatops run release prepare
```

Or alternatively, to prepare a patch release for a specific version:

```
/chatops run release prepare 16.11.3
```

The command will generate a blog post with the unreleased merge requests for the single version on the [www-gitlab-com] repository.

# Utilities

## List bug fixes waiting to be released

Release managers can make usage of a ChatOps command to fetch the list of backports waiting to be released. For this:

1. Release managerse can execute the `/chatops run release pending_backports` command on Slack.
1. Release-tools will fetch the unreleased merge requests merged into the last three active stable branches.
1. A message will be posted on Slack listing the merge requests that will be included in the next patch release

| Example |
| ---- |
| ![pending backports](images/pending_backports.png) |

## Stable branches configuration

Stable branches are configured to respect the [maintenance policy] for bug fixes:

* Engineers can backport bug fixes into the current stable branch following the [runbook for GitLab engineers].
* Stable branches outside the [maintenance policy] for bug fixes are restricted to release managers. GitLab
  engineers require a [backport request] to be approved by release managers to backport a high-priority bug
  fix into an older stable branch.

Stable branches configuration[^1] consists of:

1. A protected branch rule for the current stable branch allowing `merge` access to maintainers.
1. A wildcard that covers stable branches limiting the `push` and `merge` access to release managers.
1. A merge request approval rule that enforces maintainer approval on merge requests targeting stable branches[^2].

| [Protected branch rules] | [Merge request approval rules] |
| ----- | ----- |
| ![protected branch](images/protected_branch_rules.png) | ![merge request approval rules](images/merge_request_approval_rules.png) |


Stable branches permissions are adjusted by release managers at the end of each release.

[^1]: Rules 1 and 2 are configured on GitLab and Omnibus projects, the remaining projects under [Managed Versioning] allow maintainers to merge into any stable branch.
[^2]: This rule is only implemented on the [GitLab project]

[backport request]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[Bug remediation SLO]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity-slos
[Managed Versioning]: components/managed-versioning/index.md#components-under-managed-versioning
[Merge request approval rules]: https://gitlab.com/gitlab-org/gitlab/-/settings/merge_requests
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[GitLab project]: https://gitlab.com/gitlab-org/gitlab/
[overview of the planned patch release]: ./readme.md#planned-patch-release
[patch releases for a single version]: #patch-release-for-a-single-version
[Protected branch rules]: https://gitlab.com/gitlab-org/gitlab/-/settings/repository
[release managers]: https://about.gitlab.com/community/release-managers/
[Release SLO]: https://internal.gitlab.com/handbook/company/performance-indicators/product/saas-platforms-section/#deliveryreleases---mean-time-between-security-releases
[release dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[release task tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[runbook for GitLab engineers]: ./engineers.md
[unplanned critical patches]: #unplanned-critical-patch-release
[Unplanned critical patch releases]: ./readme.md#unplanned-critical-patch-release
[Vulnerability remediation SLA]: https://handbook.gitlab.com/handbook/security/threat-management/vulnerability-management/#remediation-slas
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
[www-gitlab-com security]: https://gitlab.com/gitlab-org/security/www-gitlab-com
