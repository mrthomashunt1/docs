# Patch Releases

Patch releases are performed according to the [GitLab Maintenance Policy], they include bug fixes for the current
stable released version of GitLab and security fixes for the current stable and previous two GitLab versions.

At GitLab, we have two types of patch releases:

1. [Planned] (default): A planned patch release to publish all available bug and vulnerability fixes per the [GitLab Maintenance Policy].
    This is an SLO driven type that take place around the [monthly release].
2. [Unplanned critical]: An unplanned, immediate patch and mitigation is required.

Planned patch releases are scheduled twice a month on the second and fourth Wednesdays, around the monthly release week,
to meet the [bug SLO] and [Security Remediation SLAs]. These are best-effort dates and they might be subject to change.

Non-security patches that are outside of our the maintenance policy for bug
fixes must be requested and agreed upon by the Release Managers and the requester (see
[backporting to versions outside the Maintenance Policy] for details).

Once the release managers begin preparing a patch release they may make the decision to remove any lower severity MRs.
This is to avoid an S1 fix, or large number of other fixes from being delayed by a lower severity fix.

* [GitLab engineer runbook for backporting bug fixes]
* [GitLab engineer runbook for preparing security fixes]
* [Process for Release Managers]
* [Release Information Grafana dashboard]

## Planned patch release

* **Note** For the current information and status of the active patch release, have a look at the [Release Information Grafana dashboard](https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1).

The planned patch release includes all bug and security fixes ready at the time of the patch release preparation. Bug fixes are
worked on in the canonical GitLab repositories, while security fixes are worked on in the mirrored GitLab security repositories
to avoid revealing vulnerabilities before the release.

The end-to-end process consists of the following stages:

![patch release diagram](./images/patch_release_diagram.jpg)

[Source] - Internal link only

At any given time, GitLab Engineers prepare bug fixes for the current version and vulnerability fixes
to the current and previous two GitLab versions:

- **1a. Bug fixes are prepared** - A merge request backporting a bug fix to the current version is prepared by GitLab Engineers.
- **1b. Vulnerability fix prepared** - GitLab engineers fix vulnerabilities in the relevant [Security repositories].

Two days before the planned due date, release managers start the patch release process, they make sure that
all prepared bug and security fixes are safely released. Deployments to GitLab.com run in parallel.

A patch release has the following phases:

- **2. First steps** Release preparation begins when release managers run the prepare chatops command to create the new
    release task issue to guide the patch release. From here they follow the checklist to complete the initial set up and 
    communication issues needed to prepare the release.
- **3. Early Merge Phase** - Release Managers deploy security fixes to GitLab.com. Fixes with the ~"security-target" label
    that are linked to the security tracking issue will have the MR targeting the default branch merged. This allows fixes
    to be deployed toGitLab.com before they are released to self-managed users.
- **4. Merge backports** - The day before the release due date, backports with security fixes targeting the supported versions are merged.
     At this point, everything included in the patch must be deployed to GitLab.com, and backports must apply to all stable branches.
- **5. Release preparation** -  When all fixes are deployed and merged, Release managers prepare, test and publish the packages.
- **6. Final steps** - At this point patch release packages are available to all users. Release managers wrap up the final steps of the patch release
    by publishing the blog post and syncing to Canonical to return to our default state of working in the open.

Details of step 1a can be seen on [GitLab engineer runbook for backporting bug fixes] and details of step 1b can be found on
[GitLab engineer runbook for preparing security fixes], and details for steps 4 and 7 can be found on [Process for Release Managers]

## Unplanned critical patch release

Unplanned critical patch releases are ad-hoc processes used to immediately patch and mitigate a high-severity vulnerability in order to meet the [Security Remediation SLAs].
Following the [GitLab Maintenance Policy], the vulnerability will be fixed in all supported versions following [Security Remediation SLAs].
The AppSec team is responsible for assessing the vulnerability and working with development to decide on the best approach to resolve it. If an unplanned critical
patch release is needed the AppSec engineer will work with Release Managers to agree on a timeline for the release.

---

[Return to Guides](../README.md#guides)

[backporting to versions outside the Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[bug SLO]: https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/issue-triage/#severity-slos
[GitLab engineer runbook for backporting bug fixes]: engineers.md
[GitLab engineer runbook for preparing security fixes]: ../security/engineer.md
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[monthly release]: https://about.gitlab.com/releases/
[overview]: #overview
[planned]: #planned-patch-release
[Process for Release Managers]: release_managers.md
[Release Information Grafana dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[Source]: https://docs.google.com/presentation/d/12JXlLnZ8lQp7ATdaSoL4x_oCUv04rmqzYp6dQb8AXHE/edit#slide=id.g2d0bc50ab08_0_5
[security repositories]: https://gitlab.com/gitlab-org/security/
[Security Remediation SLAs]: https://handbook.gitlab.com/handbook/security/threat-management/vulnerability-management/#remediation-slas
[unplanned critical]: #unplanned-critical-patch-release
