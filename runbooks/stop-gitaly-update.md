# Stop Gitaly Update

Gitaly version is updated in the [GitLab repository](https://gitlab.com/gitlab-org/gitlab) via a [scheduled pipeline] in `release-tools` in `ops.gitlab.net`, listed as `components:update_gitaly`. If for some reasons (bug, maintenance, etc.), Gitaly should not be updated, please disable this [scheduled pipeline].

[scheduled pipeline]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
